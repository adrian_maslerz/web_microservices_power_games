import { Product } from './product.interface';

export interface CartElement
{
    amount: number
    product: Product
}
