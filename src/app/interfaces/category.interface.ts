export interface Category
{
    _id: string;
    title: string;
    parent?: Category;
    products_count?: number;
    created: number;
}
