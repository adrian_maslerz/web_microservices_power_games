import { Category } from './category.interface';

export interface Product
{
    _id: string;
    languages: string[];
    game_modes: string[];
    category: Category
    title: string;
    image: string;
    manufacturer: string;
    availability: number;
    intro: string;
    description: string;
    price: number;
    age_category: string;
    release_type: string;
    premiere: number;
    created: number;
}
