
export interface ProductCopy
{
    _id: string;
    original_id: string;
    title: string;
    image: string;
    price: number;
    premiere: number;
}
