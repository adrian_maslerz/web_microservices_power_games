import { Component } from '@angular/core';
import { ProductsDataService } from '../../../services/data/products.data.service';

@Component({
    selector: 'app-games',
    templateUrl: './games.component.html',
    styleUrls: ['./games.component.scss'],
    providers: [ ProductsDataService ]
})
export class GamesComponent {}
