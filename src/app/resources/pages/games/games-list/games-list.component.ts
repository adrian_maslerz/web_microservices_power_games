import { Component, OnDestroy, OnInit } from '@angular/core';
import { ProductsDataService } from '../../../../services/data/products.data.service';
import { Product } from '../../../../interfaces/product.interface';
import { Category } from '../../../../interfaces/category.interface';
import { FormControl, FormGroup } from '@angular/forms';
import { merge, Subscription } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { handleValidationStateClass } from '../../../../utilities/form.utils';
import { GamesStateService } from '../../../../services/state/games.state.service';

@Component({
    selector: 'app-games-list',
    templateUrl: './games-list.component.html',
    styleUrls: ['./games-list.component.scss']
})
export class GamesListComponent implements OnInit, OnDestroy
{
    public category: Category;
    public products: Product[] = [];
    public page: number = 1;
    public total: number = 0;
    public readonly results: number = 20;

    public ageCategories: string[] = [ "7", "12", "16", "18", "No information" ];
    public languages: string[] = [ "Polish", "English", "German", "French", "Spanish", "Italian", "Japanese" ];
    public gameModes: string[] = [ "Single player", "Multiplayer", "Massive online multiplayer" ];
    public releaseTypes: string[] = [ "Basic", "DLC addition", "GOTY edition", "VR" ];
    public sorts: { name: string, value: string }[] = [
        { name: "Title ascending", value: "titleASC" },
        { name: "Title descending", value: "titleDESC" },
        { name: "Manufacturer ascending", value: "manufacturerASC" },
        { name: "Manufacturer descending", value: "manufacturerDESC" },
        { name: "Price ascending", value: "priceASC" },
        { name: "Price descending", value: "priceDESC" },
        { name: "Premiere ascending", value: "premiereASC" },
        { name: "Premiere descending", value: "premiereDESC" }
    ]

    public form: FormGroup;
    private subscription: Subscription = new Subscription();
    public formUtils = { handleValidationStateClass };

    constructor(
        private productsDataService: ProductsDataService,
        private gamesStateService: GamesStateService
    ) { }

    ngOnInit(): void
    {
        //form init
        this.form = new FormGroup({
            search: new FormControl(""),
            price_from: new FormControl(null),
            price_to: new FormControl(null),
            premiere_from: new FormControl(null),
            premiere_to: new FormControl(null),
            age_categories: new FormControl(null),
            languages: new FormControl(null),
            game_modes: new FormControl(null),
            release_types: new FormControl(null),
            sort: new FormControl('titleASC'),
        });

        //saved params init
        const params = this.gamesStateService.getParams();
        this.form.patchValue(params);
        if(params['category'])
            this.category = params['category'];

        this.getData(true);

        //listening to changes
        this.subscription.add(
            merge(
                this.form.get("search").valueChanges.pipe(debounceTime(400)),
                this.form.get("price_from").valueChanges.pipe(debounceTime(400)),
                this.form.get("price_to").valueChanges.pipe(debounceTime(400)),
                this.form.get("premiere_from").valueChanges,
                this.form.get("premiere_to").valueChanges,
                this.form.get("age_categories").valueChanges,
                this.form.get("age_categories").valueChanges,
                this.form.get("languages").valueChanges,
                this.form.get("game_modes").valueChanges,
                this.form.get("release_types").valueChanges
            )
            .pipe(debounceTime(100))
            .subscribe(() => {
                this.gamesStateService.updateParams({...this.form.value, category: this.category });
                this.page = 1;
                this.getData(true);
            })
        )
    }

    ngOnDestroy(): void {
        if(this.subscription)
            this.subscription.unsubscribe();
    }

    public onScroll(): void
    {
        this.page++;
        this.getData();
    }

    public onClear(): void
    {
        this.gamesStateService.clearParams();
        this.form.reset();
        this.form.get("search").patchValue("");
        this.form.get("sort").patchValue("titleASC");
    }

    public onSort(): void
    {
        this.page = 1;
        this.gamesStateService.updateParams({...this.form.value, category: this.category });
        this.getData(true);
    }

    public onSelectedCategory(category: Category | null): void
    {
        this.category = category;
        this.gamesStateService.updateParams({...this.form.value, category: this.category });
        this.page = 1;
        this.getData(true);
    }

    private getData(init: boolean = false): void
    {
        this.productsDataService
            .getProducts({
                page: this.page,
                results: this.results,
                category: this.category ? this.category._id : null,
                ...this.form.value
            })
            .subscribe(results => {
                this.products = init ? results.results : [...this.products, ...results.results ];
                this.total = results.total;
            })
    }
}
