import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { UserDataService } from '../../../../../services/data/user.data.service';
import { phoneNumber } from '../../../../../utilities/validators';
import { getMessagesConfig } from '../../../../../utilities/form.utils';
import { Router } from '@angular/router';
import { User } from '../../../../../interfaces/user.interface';

@Component({
    selector: 'app-edit-profile',
    templateUrl: './edit-profile.component.html',
    styleUrls: ['./edit-profile.component.scss']
})
export class EditProfileComponent implements OnInit
{
    public form: FormGroup;
    public inProgress: boolean = false;
    public messages: any = [];

    constructor(
        private dialog: MatDialog,
        private userDataService: UserDataService,
        private snackBar: MatSnackBar,
        private router: Router
    ) { }

    ngOnInit(): void
    {
        //init form
        this.form = new FormGroup({
            first_name: new FormControl(null, [Validators.required, Validators.maxLength(50)]),
            last_name: new FormControl(null, [Validators.required, Validators.maxLength(50)]),
            phone_number: new FormControl(null, [Validators.required, Validators.maxLength(50), phoneNumber ]),
            postal_code: new FormControl(null, [Validators.required, Validators.maxLength(30)]),
            address_line_1: new FormControl(null, [Validators.required, Validators.maxLength(50)]),
            address_line_2: new FormControl(null, [Validators.maxLength(50)]),
            city: new FormControl(null, [Validators.required, Validators.maxLength(50)]),
            country: new FormControl(null, [Validators.maxLength(50)]),
        });

        //messages config
        const fieldsConfig = {
            first_name: ['required', { key: "maxlength", value: 50}],
            last_name: ['required', { key: "maxlength", value: 50}],
            phone_number: ['required', { key: "maxlength", value: 50}, 'phoneNumber'],
            postal_code: ['required', { key: "maxlength", value: 30}],
            address_line_1: ['required', { key: "maxlength", value: 50}],
            address_line_2: [{ key: "maxlength", value: 50}],
            city: ['required', { key: "maxlength", value: 50}],
            country: [{ key: "maxlength", value: 50}]
        };
        this.messages = getMessagesConfig(fieldsConfig);

        //populating data
        this.getData();
    }

    public onSubmit(): void
    {
        //handle invalid
        if (!this.form.valid)
            return this.form.markAllAsTouched();

        this.inProgress = true;
        this.userDataService
            .updateUser(this.form.value)
            .subscribe(() =>
                {
                    this.inProgress = false;
                    this.snackBar.open("Profile updated successfully!");
                    this.router.navigate(["/logged/profile"]);
                },
                () => {
                    this.inProgress = false;
                });
    }

    public getData(): void
    {
        this.userDataService
            .getUser()
            .subscribe((user: User) => {
                this.form.patchValue({
                    first_name: user.first_name,
                    last_name: user.last_name,
                    phone_number: user.phone_number,
                    postal_code: user.address.postal_code,
                    address_line_1: user.address.address_line_1,
                    address_line_2: user.address.address_line_2,
                    city: user.address.city,
                    country: user.address.country
                })
            });
    }
}
