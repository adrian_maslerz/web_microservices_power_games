import { Component } from '@angular/core';
import { OrdersDataService } from '../../../../services/data/orders.data.service';

@Component({
    selector: 'app-orders',
    templateUrl: './orders.component.html',
    styleUrls: ['./orders.component.scss'],
    providers: [ OrdersDataService ]
})
export class OrdersComponent
{
    constructor() { }
}
