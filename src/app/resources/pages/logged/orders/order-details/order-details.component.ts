import { Component, OnInit } from '@angular/core';
import { Order } from '../../../../../interfaces/order.interface';
import { Payment } from '../../../../../interfaces/payment.interface';
import { ActivatedRoute } from '@angular/router';
import { OrdersDataService } from '../../../../../services/data/orders.data.service';
import { PaymentsDataService } from '../../../../../services/data/payments.data.service';
import { flatMap } from 'rxjs/operators';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { OrderItem } from '../../../../../interfaces/order-item.interface';
import { numeric } from '../../../../../utilities/validators';
import { getMessagesConfig, handleValidationErrorMessage } from '../../../../../utilities/form.utils';

@Component({
    selector: 'app-order-details',
    templateUrl: './order-details.component.html',
    styleUrls: ['./order-details.component.scss'],
    providers: [ PaymentsDataService ]
})
export class OrderDetailsComponent implements OnInit
{
    public order: Order;
    public payment: Payment;
    private id: string = "";

    public forms: FormGroup[] = [];
    public inProgress: boolean = false;
    public messages: any = [];
    public formUtils = { handleValidationErrorMessage };

    constructor(
        private route: ActivatedRoute,
        private ordersDataService: OrdersDataService,
        private paymentsDataService: PaymentsDataService,
        private snackBar: MatSnackBar
    ) { }

    ngOnInit(): void
    {
        //getting route param
        this.id = this.route.snapshot.params['id'];

        //getting data
        this.getData();

        //messages config
        const fieldsConfig = {
            rate: ['required', 'numeric', { key: "min", value: 1}, { key: "max", value: 5}],
            description: ['required', { key: "maxlength", value: 500}],
        };
        this.messages = getMessagesConfig(fieldsConfig);
    }

    private initForm(): void
    {
        //init form
        const form = new FormGroup({
            rate: new FormControl(null, [Validators.required, Validators.min(1), Validators.max(5), numeric ]),
            description: new FormControl(null, [Validators.required, Validators.maxLength(500)]),
        });

        this.forms.push(form);
    }

    public onSubmit(form: FormGroup, item: OrderItem): void
    {
        //handle invalid
        if (!form.valid)
            return form.markAllAsTouched();

        this.inProgress = true;
        this.ordersDataService
            .addOrderItemReview(this.order._id, item._id, form.value)
            .subscribe(() =>
                {
                    this.inProgress = false;
                    this.snackBar.open("Review added successfully!");
                    this.getData();
                },
                () => {
                    this.inProgress = false;
                });
    }

    private getData(): void
    {
        //getting order
        this.ordersDataService
            .getOrder(this.id)
            .pipe(flatMap((order: Order) => {
                this.forms = [];
                this.order = order;
                order.items.forEach(item => this.initForm());
                return this.paymentsDataService.getPayment(this.order.payment);
            }))
            .subscribe((payment: Payment) => this.payment = payment);
    }
}
