import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { handleValidationErrorMessage, handleValidationStateClass } from '../../../../utilities/form.utils';

@Component({
  selector: 'app-textarea-input',
  templateUrl: './textarea-input.component.html',
  styleUrls: ['./textarea-input.component.scss']
})
export class TextareaInputComponent {

    @Input() form: FormGroup;
    @Input() name: string;
    @Input() displayName: string = this.name;
    @Input() messages: any[] = [];
    @Input() rows: number = 10;

    public formUtils = { handleValidationStateClass, handleValidationErrorMessage };

    constructor() { }
}
