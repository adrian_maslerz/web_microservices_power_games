import { Component, Input, ChangeDetectionStrategy, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
    selector: 'app-table-sort',
    templateUrl: './table-sort.component.html',
    styleUrls: ['./table-sort.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class TableSortComponent
{
    @Input() key: string;
    @Input() set activeSort(sort: string) {
        if(sort && sort.replace("ASC","").replace("DESC", "") == this.key)
            this.option = <'' | 'ASC' | 'DESC'>sort.replace(this.key,"");
        else
            this.option = '';
    }
    @Output() sort: EventEmitter<string> = new EventEmitter();

    public option: '' | 'ASC' | 'DESC' = '';
    constructor() { }

    public onSort(): void
    {
        switch (this.option)
        {
            case 'ASC':
                this.option = 'DESC';
                break;
            case 'DESC':
                this.option = '';
                break;
            default:
                this.option = 'ASC';
                break;
        }

        this.sort.emit(this.key + this.option);
    }
}
