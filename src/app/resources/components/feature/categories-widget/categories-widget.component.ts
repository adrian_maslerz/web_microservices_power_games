import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Category } from '../../../../interfaces/category.interface';
import { CategoriesDataService } from '../../../../services/data/categories.data.service';

@Component({
    selector: 'app-categories-widget',
    templateUrl: './categories-widget.component.html',
    styleUrls: ['./categories-widget.component.scss'],
    providers: [ CategoriesDataService ]
})
export class CategoriesWidgetComponent implements OnInit
{
    @Input() parent: Category = null;
    @Output() selectedCategory: EventEmitter<Category | null> = new EventEmitter();

    public categories: Category[] = [];
    public total: number = 0;
    public page: number = 1;
    public readonly results: number = 10;

    constructor(
        private categoriesDataService: CategoriesDataService
    ) { }

    ngOnInit(): void
    {
        this.getData(true);
    }

    public onCategoryChosen(category: Category | null): void
    {
        this.page = 1;
        this.parent = category || null;
        this.selectedCategory.emit(this.parent);
        this.getData(true);
    }

    public onLoadMore(): void
    {
        this.page++;
        this.getData();
    }

    private getData(init: boolean = false): void
    {
        this.categoriesDataService
            .getCategories({
                page: this.page,
                results: this.results,
                parent: this.parent ? this.parent._id : null
            })
            .subscribe(results => {
                this.categories = init ? results.results : [...this.categories, ...results.results ];
                this.total = results.total;
            })
    }
}
