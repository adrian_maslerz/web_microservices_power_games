import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ModalLoginComponent } from '../modal-login/modal-login.component';
import { AuthService } from '../../../../services/core/auth.service';
import { Subscription } from 'rxjs';
import { User } from '../../../../interfaces/user.interface';
import { Router } from '@angular/router';
import { CartStateService } from '../../../../services/state/cart.state.service';
import { CartElement } from '../../../../interfaces/cart-element.interface';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy
{
    public user: User;
    public isCollapsed: boolean = true;
    public cartElements: CartElement[] = [];
    private subscription: Subscription = new Subscription();

    constructor(
        private dialog: MatDialog,
        private router: Router,
        private authService: AuthService,
        private cartStateService: CartStateService
    ) {}

    ngOnInit(): void
    {
        this.subscription.add(this.authService
            .getUserSubscription()
            .subscribe((user: User) => this.user = user));

        this.subscription.add(this.cartStateService
            .cartElementsSubscription
            .subscribe((elements: CartElement[]) => {
                this.cartElements = elements;
            }));
    }
    ngOnDestroy(): void
    {
        if(this.subscription)
            this.subscription.unsubscribe();
    }

    public onLoginModal(): void
    {
        this.dialog.open(ModalLoginComponent);
    }

    public onLogout(): void
    {
        this.authService.logout();
        this.router.navigate(["/"]);
    }
}
