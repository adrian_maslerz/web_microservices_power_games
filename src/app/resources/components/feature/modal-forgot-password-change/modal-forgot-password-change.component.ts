import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthDataService } from '../../../../services/data/auth.data.service';
import { getMessagesConfig } from '../../../../utilities/form.utils';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { equalToFieldValue, password } from '../../../../utilities/validators';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    selector: 'app-modal-forgot-password-change',
    templateUrl: './modal-forgot-password-change.component.html',
    styleUrls: ['./modal-forgot-password-change.component.scss'],
    providers: [ AuthDataService ]
})
export class ModalForgotPasswordChangeComponent implements OnInit
{
    private token: string = "";
    public form: FormGroup;
    public inProgress: boolean = false;
    public messages: any = [];

    constructor(
        private dialogRef: MatDialogRef<ModalForgotPasswordChangeComponent>,
        private dialog: MatDialog,
        private authDataService: AuthDataService,
        private snackBar: MatSnackBar,
        private route: ActivatedRoute,
        private router: Router
    ) {}

    ngOnInit(): void
    {
        //getting token
        this.token = this.route.snapshot.queryParams["token"];

        //init form
        this.form = new FormGroup({
            password: new FormControl(null, [Validators.required, Validators.minLength(8), password]),
            repeated_password: new FormControl(null, [Validators.required])
        });

        //messages config
        const fieldsConfig = {
            password: ['required', { key: 'minlength', value: 8 }, 'password', { key: 'equalToFieldValue', message: 'Passwords don\'t match.' }],
            repeated_password: ['required', { key: 'equalToFieldValue', message: 'Passwords don\'t match.' }],
        };
        this.messages = getMessagesConfig(fieldsConfig);

        // password
        this.form.get('password')
            .valueChanges
            .subscribe(
                () =>
                {
                    const control = this.form.get('repeated_password');
                    control.setValidators([Validators.required, equalToFieldValue(this.form.get('password').value)]);
                    control.updateValueAndValidity();
                });

        this.form.get('repeated_password')
            .valueChanges
            .subscribe(() => this.form.get('repeated_password').setValidators([Validators.required, equalToFieldValue(this.form.get('password').value)]));
    }

    public onSubmit(): void
    {
        //handle invalid
        if (!this.form.valid)
            return this.form.markAllAsTouched();

        this.inProgress = true;
        this.authDataService
            .resetPasswordChange(this.form.value, this.token)
            .subscribe(() =>
                {
                    this.inProgress = false;
                    this.snackBar.open('Successfully changed password!');
                    this.dialogRef.close();
                    this.router.navigate(["/"]);
                },
                () =>
                {
                    this.inProgress = false;
                });
    }

}
