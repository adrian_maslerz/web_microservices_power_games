import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GamesComponent } from './resources/pages/games/games.component';
import { GamesListComponent } from './resources/pages/games/games-list/games-list.component';
import { GameDetailsComponent } from './resources/pages/games/game-details/game-details.component';
import { HomeComponent } from './resources/pages/home/home.component';
import { ContactComponent } from './resources/pages/contact/contact.component';
import { CartComponent } from './resources/pages/cart/cart.component';
import { PaymentComponent } from './resources/pages/payment/payment.component';
import { AuthGuard } from './guards/auth.guard';
import { OrderSummaryComponent } from './resources/pages/order-summary/order-summary.component';
import { LoggedComponent } from './resources/pages/logged/logged.component';
import { ProfileComponent } from './resources/pages/logged/profile/profile.component';
import { ProfileDetailsComponent } from './resources/pages/logged/profile/profile-details/profile-details.component';
import { EditProfileComponent } from './resources/pages/logged/profile/edit-profile/edit-profile.component';
import { ChangePasswordComponent } from './resources/pages/logged/profile/change-password/change-password.component';
import { ChangeEmailComponent } from './resources/pages/logged/profile/change-email/change-email.component';
import { OrdersComponent } from './resources/pages/logged/orders/orders.component';
import { OrdersListComponent } from './resources/pages/logged/orders/orders-list/orders-list.component';
import { OrderDetailsComponent } from './resources/pages/logged/orders/order-details/order-details.component';

const routes: Routes = [

    //logged
    {
        path: 'logged', component: LoggedComponent, canActivate: [AuthGuard], children: [
            {
                path: 'profile', component: ProfileComponent, children: [
                    { path: '', component: ProfileDetailsComponent },
                    { path: 'edit', component: EditProfileComponent },
                    { path: 'change-password', component: ChangePasswordComponent },
                    { path: 'change-email', component: ChangeEmailComponent }
                ]
            },
            {
                path: 'orders', component: OrdersComponent, children: [
                    { path: '', component: OrdersListComponent },
                    { path: ':id', component: OrderDetailsComponent }
                ]
            }
        ]
    },

    //home
    { path: '', component: HomeComponent },
    { path: 'password-reset', component: HomeComponent },

    //contact
    { path: 'contact', component: ContactComponent },

    //order process
    { path: 'cart', component: CartComponent },
    { path: 'payment', canActivate: [AuthGuard], component: PaymentComponent },
    { path: 'summary/:id', canActivate: [AuthGuard], component: OrderSummaryComponent },

    //games
    {
        path: 'games', component: GamesComponent, children: [
            { path: '', component: GamesListComponent },
            { path: ':id', component: GameDetailsComponent },
        ]
    },
    { path: '**', redirectTo: '/' }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {}
