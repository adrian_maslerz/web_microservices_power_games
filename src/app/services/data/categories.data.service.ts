import { Injectable } from '@angular/core';
import { ApiService } from '../core/api.service';
import { Observable } from 'rxjs/index';
import { Pagination } from '../../interfaces/pagination.interface';
import { Category } from '../../interfaces/category.interface';

@Injectable()
export class CategoriesDataService
{
    constructor(private apiService: ApiService) { }

    public getCategories(data: any) : Observable<Pagination<Category>>
    {
        return this.apiService.get("products","/categories", data);
    }
}
