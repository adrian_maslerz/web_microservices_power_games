import { Injectable } from '@angular/core';
import { ApiService } from '../core/api.service';
import { Observable } from 'rxjs/index';
import { Pagination } from '../../interfaces/pagination.interface';
import { Product } from '../../interfaces/product.interface';

@Injectable()
export class ProductsDataService
{
    constructor(private apiService: ApiService) { }

    public getProducts(data: any) : Observable<Pagination<Product>>
    {
        //preparing data
        if(data["premiere_from"] && !isNaN(Date.parse(data["premiere_from"])))
            data["premiere_from"] = new Date(data["premiere_from"]).getTime();

        if(data["premiere_to"] && !isNaN(Date.parse(data["premiere_to"])))
            data["premiere_to"] = new Date(data["premiere_to"]).getTime();

        if(data["languages"])
            data["languages"] = JSON.stringify(data["languages"]);
        if(data["game_modes"])
            data["game_modes"] = JSON.stringify(data["game_modes"]);
        if(data["age_categories"])
            data["age_categories"] = JSON.stringify(data["age_categories"]);
        if(data["release_types"])
            data["release_types"] = JSON.stringify(data["release_types"]);

        return this.apiService.get("products","/products", data);
    }

    public getHomeProducts() : Observable<{ newest: Product[], premieres: Product[], recommended: Product[] }>
    {
        return this.apiService.get("products","/products/home");
    }

    public getProduct(id: string) : Observable<Product>
    {
        return this.apiService.get("products", "/products/" + id);
    }
}
