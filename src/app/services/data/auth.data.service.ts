import { Injectable } from '@angular/core';
import { ApiService } from '../core/api.service';
import { Observable } from 'rxjs/index';
import { map } from 'rxjs/internal/operators';


import { AuthService } from '../core/auth.service';
import { SuccessInterface } from '../../interfaces/success.interface';
import { User } from '../../interfaces/user.interface';
import { Auth } from '../../interfaces/auth.interface';

@Injectable()
export class AuthDataService
{
    constructor(private apiService: ApiService, private authService: AuthService) { }

    public login(data: { email: string, password: string }) : Observable<any>
    {

        return this.apiService.post("users","/login", data).pipe(map((data: { user: User, auth: Auth }) => {
            this.authService.login(data.user, data.auth);
            return data;
        }));
    }

    public register(data: any) : Observable<any>
    {
        return this.apiService.post("users","/register", data).pipe(map((data: { user: User, auth: Auth }) => {
            this.authService.login(data.user, data.auth);
            return data;
        }));
    }

    public resetPassword(data: { email: string }) : Observable<SuccessInterface>
    {
        return this.apiService.post("users","/password/remind", data);
    }

    public resetPasswordChange(data: { password: string, repeated_password: string }, token: string) : Observable<SuccessInterface>
    {
        return this.apiService.put("users","/password/remind", { password: data.password, token: token });
    }
}
