import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class GamesStateService
{
    private storage = sessionStorage;
    private storageKey: string = "games";
    private params: Object = {};

    constructor()
    {
        var params: Object;
        try
        {
            params = JSON.parse(this.storage.getItem(this.storageKey));
            if(params)
            {
                this.params = params;
            }
        }
        catch (error) {}
    }

    public getParams(): Object
    {
        return this.params;
    }

    public updateParams(params): void
    {
        this.params = params;
        this.storage.setItem(this.storageKey, JSON.stringify(this.params));
    }

    public clearParams(): void
    {
        this.params = {};
        this.storage.setItem(this.storageKey, JSON.stringify(this.params));
    }
}
