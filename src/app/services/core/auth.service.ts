import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { JwtHelperService } from '@auth0/angular-jwt';
import { User } from '../../interfaces/user.interface';
import { Auth } from '../../interfaces/auth.interface';

@Injectable({
    providedIn: 'root'
})
export class AuthService
{
    private user: User;
    private auth: Auth;
    private storage = sessionStorage;
    private storageKey: string = "authData";
    private authenticated: boolean = false;
    private userSubject: BehaviorSubject<User> = new BehaviorSubject<User>(null);

    constructor()
    {
        var authData: { user: User, auth: Auth };
        try
        {
            authData = JSON.parse(<string>this.storage.getItem(this.storageKey));
            this.login(authData.user, authData.auth);
        }
        catch (error) {}
    }

    public login(user: User, auth: Auth)
    {
        this.storage.setItem(this.storageKey, JSON.stringify({ user: user, auth: auth }));
        this.auth = auth;
        this.user = user;
        this.userSubject.next(this.user);
        this.authenticated = true;
    }

    public logout()
    {
        this.storage.removeItem(this.storageKey);
        this.auth = undefined;
        this.user = undefined;
        this.userSubject.next(this.user);
        this.authenticated = false;
    }

    public getLoggedUser() : User | undefined
    {
        return this.user;
    }

    public updateLoggedUser(user: User)
    {
        this.user = user;
        this.storage.setItem(this.storageKey, JSON.stringify({ user: user, auth: this.auth }));
        this.userSubject.next(this.user);
    }

    public getAuthToken(): Auth
    {
        return this.auth;
    }

    public isAuthenticated() : boolean
    {
        return this.authenticated;
    }

    public getUserSubscription() : Observable<User>
    {
        return this.userSubject.asObservable();
    }

    public getUserPermissions(): Array<string>
    {
        const helper = new JwtHelperService();
        const auth = this.getAuthToken();
        const decodedToken = helper.decodeToken(auth?.token);
        return decodedToken && decodedToken.data && Array.isArray(decodedToken.data.permissions) ? decodedToken.data.permissions : [];
    }
}
