import { FormControl, FormGroup } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/index';

export function handleUniqueValidator(method: Function)
{
    return (control: FormControl): Promise<any> | Observable<any> =>
    {
        return new Promise<any>(((resolve, reject) =>
        {
            method(control.value).subscribe((status) => resolve(status[ 'exists' ] ? { uniqueness: true }: null));
        }));
    };
}

export function handleValidationStateClass(form: FormGroup, field: string): { 'is-invalid': boolean, 'is-valid': boolean }
{
    return {
        'is-invalid': !form.get(field).valid && form.get(field).touched,
        'is-valid': form.get(field).valid && form.get(field).touched
    };
}

export function handleValidationErrorMessage(form: FormGroup, field, messages: Array<{ field: string, errors: Array<{ error: string, message: string }> }>): string
{
    let message = '';

    if (!form.get(field).valid && form.get(field).touched && form.get(field).errors)
    {
        const validationMessagePack = messages.find(messageObject => messageObject.field === field);
        if (validationMessagePack)
        {
            const validationMessage = validationMessagePack.errors.find(
                errorPack => errorPack.error === Object.keys(form.get(field).errors)[ 0 ]);
            message = validationMessage ? validationMessage.message: '';
        }
    }

    return message;
}

export function getMessageError(error: HttpErrorResponse): string
{
    let message = '';

    const errorsArray = error.error.errors || [];
    if (errorsArray.length)
    {
        message = errorsArray[ 0 ].message || '';
    }
    return message;
}

export function getMessagesConfig(fieldsConfig)
{
    const errorsConfig = {
        required: "{KEY} is required",
        requiredTrue: "{KEY} must be accepted",
        email: "This is not valid email address",
        minlength: "{KEY} must have {VALUE} characters at least",
        maxlength: "{KEY} can't be longer than {VALUE} characters",
        min: "{KEY} can't be lower than {VALUE}",
        max: "{KEY} can't be higher than {VALUE}",
        numbers: "{KEY} can hold only numeric values",
        numeric: "{KEY} can hold only numeric values",
        password: "{KEY} must contain at least one capital letter, number and special character",
        phoneNumber: "{KEY} can contain ony numeric values and dashes and have 7 characters at least",
        equalToFieldValue: "{KEY} must be equal to {VALUE}",
        invalidDate: "This is not valid date",
        fileSize: "{KEY} can't be bigger than {VALUE}",
        fileType: "Invalid file type. Valid mime types: {VALUE}",
        url: "This is not valid url"
    }

    const messages = [];
    Object.keys(fieldsConfig).forEach(key => {
        const errors = [];
        fieldsConfig[key].forEach(validator => {
            if(typeof validator == "string")
            {
                if(Object.keys(errorsConfig).includes(validator))
                {
                    const message = errorsConfig[validator].replace("{KEY}", key.charAt(0).toUpperCase() + (key.slice(1).toLowerCase().replace(/_/gm," ")))
                    errors.push({ error: validator, message: message })
                }
            }
            else if(typeof validator == "object")
            {
                if(Object.keys(errorsConfig).includes(validator.key) && validator.value != undefined)
                {
                    const message = errorsConfig[validator.key]
                        .replace("{KEY}", key.charAt(0).toUpperCase() + (key.slice(1).toLowerCase().replace(/_/gm," ")))
                        .replace("{VALUE}", validator.value);
                    errors.push({ error: validator.key, message: message })
                }
                else
                    errors.push({ error: validator.key, message: validator.message });
            }
        })
        messages.push({ field: key, errors: errors });
    });

    return messages;
}

export function timestampToDatepickerObject(timestamp: number) : { year: number, month: number, day: number }
{
    const date = new Date(timestamp);
    return { year: date.getFullYear(), month: date.getMonth() + 1, day: date.getDate() }
}

export function datePickerObjectToTimestamp(datepickerObject: { year: number, month: number, day: number }) : number
{
    const date = new Date(datepickerObject.year + "-" + datepickerObject.month + "-" + datepickerObject.day);
    return date.getTime();
}



